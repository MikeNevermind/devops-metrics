# DevOps Metrics reference implementation

This project shows how to calculate four DevOps Metrics identified in 
[DORA's 2017 State of DevOps Report](https://puppet.com/resources/whitepaper/2017-state-of-devops-report) 
using only public REST endpoints available in Atlassian's software (Jira, 
Bitbucket Server, Bamboo) and Jenkins.

The metrics are:

* Deployment frequency
* Change failure rate
* Mean time to recovery
* Change lead time

You can find more detail about each metric in DORA's report.


### Approach

To calculate the metrics, users first define a few parameters in the config file:

* **Period** - How many days the report will cover
* **Build** - The Bamboo or Jenkins build that the team uses to deploy code
* **Source code location** - A Bitbucket Server project and repository where the team works
* **Incident definition** - A JQL query that identifies which Jira issue are considered incidents

Using those values the application will:

* Find all builds completed during the specified period
* Find all incidents that were raised during the period
* Find all incidents that were closed during the period
* Find all pull requests that were merged during the period
* Link each merged pull request to a build using the commits contained in the pull request and build 
* Run some statistics over the gathered data to calculate the metrics

The above workflow is implemented in `MetricCalculator.createDevOpsReport()`, which 
is a good starting point for exploring the code.


### Potential improvements

#### Query efficiency

In most cases, each step above gathers its data with a single query. 
There are a few exceptions:

* We require a list of commits contained in each build. This requires a 
query for each Bamboo build.
* We need each pull request's first commit. This requires a query for 
each Bitbucket Server pull request.
* We need each pull request's merge commit. This requires a second query
for each Bitbucket Server pull request.

Since completed builds and merged pull requests are immutable, the results
of the queries could be cached.

#### Support multiple builds and repositories

This code expects that a team's work is contained in a single build and 
a single repository. Other implementations may want to support multiple 
builds and repositories.

#### Graph metrics over time

This code only calculates the metrics for a single point-in-time.

The real value in these metrics is tracking how they change over time.
Calculating, storing and displaying historical values could be achieved
by persisting intermediate values (builds, pull requests) and metrics
in a database or other persistent store. 

#### Handle rework

This implementation finds the first commit by considering only the commits 
currently on the pull request. However, there are cases where the first 
commit is removed from the pull request and replaced with a newer commit. 
You can query the pull request's activity to find these removed commits. 

#### Additional hardening

In order to aid with clarity, we focus on querying the systems and calculating
the metrics as simply as possible. When implementing a production-ready 
system, developers should consider the following:

* Caching query results for speed
* Persisting query results (e.g. some build systems are configured to delete old build results)
* Rate-limiting requests to avoid overloading external systems
* Error and exception handling 


### Running the build

This project was developed with [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
and [maven](https://maven.apache.org/), both of which should be familiar to developers of Atlassian apps.

To run the build with maven:

     mvn clean install 

A few tests in the build will connect to running Jira, Bamboo, Bitbucket and Jenkins servers. You can skip these
tests with:

     mvn clean install -DskipTests 

Alternatively, you can create a new `config.properties` file in `src/main/resources` using the template in `config.example.properties`.
To configure the Jenkins instance for the jenkins test, edit `src/test/resources/com/atlassian/devops/build/jenkins/jenkins-config.properties`.


### Running the tool

To run the tool, you must first create a new `config.properties` file in 
`src/main/resources` using the template in `config.example.properties`.

Then, run with maven:

     mvn clean install exec:java -DskipTests

