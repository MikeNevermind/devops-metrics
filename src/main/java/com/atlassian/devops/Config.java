package com.atlassian.devops;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.util.Objects.requireNonNull;

public class Config {

    private final Properties properties;

    public Config() {
        this("/config.properties");
    }

    public Config(String configFileName) {
        properties = new Properties();

        try (InputStream inputStream = Config.class.getResourceAsStream(configFileName)) {
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("The configuration file " + configFileName + " could not be found in the classpath");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to load properties", e);
        }
    }

    @Nullable
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    @Nonnull
    public String getRequiredProperty(String key) {
        return requireNonNull(getProperty(key), "Required property not set: " + key);
    }
}
