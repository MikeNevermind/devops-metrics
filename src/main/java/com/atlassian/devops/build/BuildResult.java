package com.atlassian.devops.build;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class BuildResult {

    private final String buildId;
    private final Date startTime;
    private final long buildDurationMs;
    private final List<String> changes;
    private final boolean isComplete;

    public BuildResult(@Nonnull String buildId, @Nonnull Date startTime, long buildDurationMs,
                       @Nonnull List<String> changes, boolean isComplete) {
        this.buildId = requireNonNull(buildId, "buildId");
        this.startTime = requireNonNull(startTime, "startTime");
        this.buildDurationMs = buildDurationMs;
        this.changes = requireNonNull(changes, "changes");
        this.isComplete = isComplete;
    }

    public long getBuildDurationMs() {
        return buildDurationMs;
    }

    @Nonnull
    public String getBuildId() {
        return buildId;
    }

    @Nonnull
    public List<String> getChanges() {
        return changes;
    }

    @Nonnull
    public Date getStartTime() {
        return startTime;
    }

    public boolean isComplete() {
        return isComplete;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("buildId", buildId)
                .toString();
    }
}
