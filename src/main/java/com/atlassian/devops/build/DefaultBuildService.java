package com.atlassian.devops.build;

import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class DefaultBuildService implements BuildService {

    private final BuildClient buildClient;

    public DefaultBuildService(BuildClient buildClient) {
        this.buildClient = buildClient;
    }

    @Override
    public List<BuildResult> findCompleteBuildsSince(String buildKey, long firstTimeStamp) {
        ImmutableList.Builder<BuildResult> buildsInPeriod = ImmutableList.builder();
        boolean done = false;
        PageRequest request = null;
        do {
            Page<BuildResult> buildResults = buildClient.listBuildResults(buildKey, request);
            for (BuildResult buildResult : buildResults.getValues()) {
                // We only look at completed builds that started within the period.
                // Builds that started _before_ the period but finished within it aren't included.
                if (buildResult.getStartTime().getTime() > firstTimeStamp) {
                    if (buildResult.isComplete()) {
                        buildsInPeriod.add(buildClient.enrichBuildCommitInfo(buildResult));
                    }
                } else {
                    // the rest of the builds are too old...
                    done = true;
                }
            }

            request = buildResults.getNextPageRequest();
        } while (!done && request != null);

        return buildsInPeriod.build();
    }
}
