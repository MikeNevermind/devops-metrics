package com.atlassian.devops.build.jenkins;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class JenkinsResponseParser {

    /**
     * Jenkins doesn't return page data with the response so we just assume it's sent us everything it has
     */
    @Nonnull
    public Page<BuildResult> parseBuildResultsList(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(json);

            JsonNode builds = node.get("allBuilds");
            if (builds == null) {
                builds = node.get("builds");
            }

            ImmutableList.Builder<BuildResult> buildListBuilder = ImmutableList.builder();
            for (JsonNode result : builds) {
                buildListBuilder.add(parseBuildResult(result));
            }

            List<BuildResult> buildList = buildListBuilder.build();

            return new PageImpl<>(0, buildList.size(), buildList, true);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing Jenkins build results list", e);
        }
    }

    @Nonnull
    private BuildResult parseBuildResult(JsonNode json) {
        String buildKey = json.get("number").asText();
        long startTime = json.get("timestamp").asLong();
        long duration = json.get("duration").asLong();
        boolean isComplete = !json.get("building").asBoolean();

        ImmutableList.Builder<String> changes = ImmutableList.builder();
        for (JsonNode change : json.at("/changeSet/items")) {
            changes.add(change.get("commitId").asText());
        }

        return new BuildResult(buildKey, new Date(startTime), duration, changes.build(), isComplete);
    }
}
