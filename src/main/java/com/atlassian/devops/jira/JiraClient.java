package com.atlassian.devops.jira;

import com.atlassian.devops.AtlassianRestClient;
import com.atlassian.devops.Config;
import com.atlassian.devops.JiraIssue;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;
import com.atlassian.devops.util.PageRequestImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;

import static java.util.Objects.requireNonNull;

public class JiraClient extends AtlassianRestClient {

    private final int defaultPageSize;
    private final JiraResponseParser responseParser;

    public JiraClient(Config config, JiraResponseParser responseParser) {
        super(config.getRequiredProperty("jira.base.url"),
                config.getRequiredProperty("jira.username"),
                config.getRequiredProperty("jira.password"));

        defaultPageSize = Integer.parseInt(config.getRequiredProperty("jira.pageSize"));
        this.responseParser = responseParser;
    }

    public Page<JiraIssue> search(@Nonnull String jql, @Nullable PageRequest pageRequest) {
        requireNonNull(jql, "jql");
        if (pageRequest == null) {
            pageRequest = new PageRequestImpl(0, defaultPageSize);
        }

        Invocation.Builder invocationBuilder = webTarget.path("search")
                .queryParam("jql", jql)
                .queryParam("startAt", pageRequest.getStart(), "maxResults", pageRequest.getLimit())
                .request(MediaType.APPLICATION_JSON_TYPE);

        String issuesJson = getAsString(invocationBuilder);
        return responseParser.parseJiraIssueList(issuesJson);
    }
}
