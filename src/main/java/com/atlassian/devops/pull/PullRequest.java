package com.atlassian.devops.pull;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class PullRequest {

    private final int pullRequestId;
    private final Date createdDate;
    private final Date closedDate;

    public PullRequest(int pullRequestId, @Nonnull Date createdDate, @Nonnull Date closedDate) {
        this.createdDate = requireNonNull(createdDate);
        this.closedDate = requireNonNull(closedDate);
        this.pullRequestId = pullRequestId;
    }

    public int getPullRequestId() {
        return pullRequestId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pullRequestId, createdDate, closedDate);
    }
}
