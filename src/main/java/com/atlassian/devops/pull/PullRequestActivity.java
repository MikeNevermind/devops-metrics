package com.atlassian.devops.pull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class PullRequestActivity {

    private final String action;
    private final String commitId;

    public PullRequestActivity(@Nonnull String action, @Nullable String commitId) {
        this.action = requireNonNull(action);
        this.commitId = commitId;
    }

    public String getAction() {
        return action;
    }

    public Optional<String> getCommitId() {
        return Optional.ofNullable(commitId);
    }
}
