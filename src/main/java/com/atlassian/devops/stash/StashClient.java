package com.atlassian.devops.stash;

import com.atlassian.devops.AtlassianRestClient;
import com.atlassian.devops.Config;
import com.atlassian.devops.pull.Commit;
import com.atlassian.devops.pull.PullRequest;
import com.atlassian.devops.pull.PullRequestActivity;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;
import com.atlassian.devops.util.PageRequestImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import static java.util.Objects.requireNonNull;

public class StashClient extends AtlassianRestClient {

    private final int defaultPageSize;
    private final StashResponseParser responseParser;

    public StashClient(Config config, StashResponseParser responseParser) {
        super(config.getRequiredProperty("stash.base.url"),
                config.getRequiredProperty("stash.username"),
                config.getRequiredProperty("stash.password"));

        defaultPageSize = Integer.parseInt(config.getRequiredProperty("stash.pageSize"));
        this.responseParser = responseParser;
    }

    public Page<PullRequest> listMergedPullRequests(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                    @Nullable PageRequest pageRequest) {
        requireNonNull(projectKey, "projectKey");
        requireNonNull(repositorySlug, "repositorySlug");
        pageRequest = defaultPageRequestIfNull(pageRequest);

        Invocation.Builder invocationBuilder = pullRequestsUri(projectKey, repositorySlug)
                .queryParam("state", "merged")
                .queryParam("start", pageRequest.getStart(), "limit", pageRequest.getLimit())
                .request(MediaType.APPLICATION_JSON_TYPE);

        String prJson = getAsString(invocationBuilder);
        return responseParser.parsePullRequestList(prJson);
    }

    public Page<PullRequestActivity> listPullRequestActivities(@Nonnull String projectKey,
                                                               @Nonnull String repositorySlug, int pullRequestId,
                                                               @Nullable PageRequest pageRequest) {
        requireNonNull(projectKey, "projectKey");
        requireNonNull(repositorySlug, "repositorySlug");
        pageRequest = defaultPageRequestIfNull(pageRequest);

        Invocation.Builder invocationBuilder = pullRequestsUri(projectKey, repositorySlug)
                .path(Integer.toString(pullRequestId)).path("activities")
                .queryParam("start", pageRequest.getStart(), "limit", pageRequest.getLimit())
                .request(MediaType.APPLICATION_JSON_TYPE);

        String activityJson = getAsString(invocationBuilder);
        return responseParser.parseActivityList(activityJson);
    }

    public Page<Commit> listPullRequestCommits(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                               int pullRequestId, @Nullable PageRequest pageRequest) {
        requireNonNull(projectKey, "projectKey");
        requireNonNull(repositorySlug, "repositorySlug");
        pageRequest = defaultPageRequestIfNull(pageRequest);

        Invocation.Builder invocationBuilder = pullRequestsUri(projectKey, repositorySlug)
                .path(Integer.toString(pullRequestId)).path("commits")
                .queryParam("start", pageRequest.getStart(), "limit", pageRequest.getLimit())
                .request(MediaType.APPLICATION_JSON_TYPE);

        String commitsJson = getAsString(invocationBuilder);
        return responseParser.parseCommitList(commitsJson);
    }

    private WebTarget pullRequestsUri(String projectKey, String repoSlug) {
        return webTarget.path("projects").path(projectKey).path("repos").path(repoSlug).path("pull-requests");
    }

    private PageRequest defaultPageRequestIfNull(PageRequest pageRequest) {
        return pageRequest == null ? new PageRequestImpl(0, defaultPageSize) : pageRequest;
    }
}
