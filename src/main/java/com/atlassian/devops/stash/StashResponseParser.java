package com.atlassian.devops.stash;

import com.atlassian.devops.pull.Commit;
import com.atlassian.devops.pull.PullRequest;
import com.atlassian.devops.pull.PullRequestActivity;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

public class StashResponseParser {

    @Nonnull
    public Page<PullRequest> parsePullRequestList(String json) {
        return parseList(json, pullRequestNode -> {
            int pullRequestId = pullRequestNode.get("id").asInt();
            Date createdDate = Date.from(Instant.ofEpochMilli(pullRequestNode.get("createdDate").asLong()));
            Date closedDate = Date.from(Instant.ofEpochMilli(pullRequestNode.get("closedDate").asLong()));
            return new PullRequest(pullRequestId, createdDate, closedDate);
        });
    }

    public Page<PullRequestActivity> parseActivityList(String json) {
        return parseList(json, activityNode -> {
            String action = activityNode.get("action").asText();
            String mergeCommit = activityNode.get("commit") == null ? null : activityNode.get("commit").get("id").asText();
            return new PullRequestActivity(action, mergeCommit);
        });
    }

    public Page<Commit> parseCommitList(String json) {
        return parseList(json, commitNode -> {
            String id = commitNode.get("id").asText();
            Date authorTimestamp = Date.from(Instant.ofEpochMilli(commitNode.get("authorTimestamp").asLong()));
            return new Commit(id, authorTimestamp);
        });
    }

    private <T> Page<T> parseList(String json, Function<JsonNode, T> transformer) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(json);

            int size = node.get("size").asInt();
            int start = node.get("start").asInt();
            int limit = node.get("limit").asInt();

            ImmutableList.Builder<T> objectListBuilder = ImmutableList.builder();
            for (JsonNode value : node.get("values")) {
                T object = transformer.apply(value);
                objectListBuilder.add(object);
            }

            List<T> objects = objectListBuilder.build();

            return new PageImpl<>(start, limit, objects, size < limit);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing json objects", e);
        }
    }
}
