package com.atlassian.devops.util;

public interface PageRequest {

    int getLimit();

    int getStart();
}
