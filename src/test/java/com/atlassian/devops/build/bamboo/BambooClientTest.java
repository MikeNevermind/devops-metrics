package com.atlassian.devops.build.bamboo;

import com.atlassian.devops.Config;
import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequestImpl;
import org.junit.Test;

public class BambooClientTest {

    private BambooClient client = new BambooClient(new Config(), new BambooResponseParser());

    @Test
    public void testlistBuildResults() {
        Page<BuildResult> buildResultPage = client.listBuildResults("BSERV-MASTER", new PageRequestImpl(3, 5));

        System.out.println(buildResultPage);
    }
}
