package com.atlassian.devops.build.bamboo;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class BambooResponseParserTest {

    private BambooResponseParser bambooResponseParser = new BambooResponseParser();

    @Test
    public void testShouldParseBambooBuildListData() {
        String bambooBuildResultsJson = readTestResource("bamboo-build-result-list.json");

        Page<BuildResult> page = bambooResponseParser.parseBuildResultsList(bambooBuildResultsJson);

        assertNotNull(page);
        assertEquals(23, page.getStart());
        assertEquals(5, page.getLimit());
        assertEquals(5, page.getSize());
        assertFalse(page.isLastPage());

        List<BuildResult> buildResults = page.getValues();
        assertNotNull(buildResults);
        assertEquals(5, buildResults.size());

        BuildResult result = buildResults.get(0);
        assertEquals(1398297L, result.getBuildDurationMs());
        assertDateEquals("2019-01-29T05:03:06.402Z", result.getStartTime());
        // We don't get changes on a bulk build result request
        assertEquals(0, result.getChanges().size());
    }

    @Test
    public void testShouldParseSingleBambooBuildWithChanges() {
        String bambooBuildResultJson = readTestResource("bamboo-single-build-result-with-changes.json");

        BuildResult result = bambooResponseParser.parseBuildResult(bambooBuildResultJson);

        assertNotNull(result);
        assertEquals(1464141L, result.getBuildDurationMs());
        assertDateEquals("2019-02-01T07:05:46.811Z", result.getStartTime());
        assertEquals(3, result.getChanges().size());
        assertEquals("3bb79d6b57f227b4ca2651556f9eb3c93e7dffbf", result.getChanges().get(0));
        assertEquals("ab9455f717acedcb034dc1d39192adf698d2c130", result.getChanges().get(1));
        assertEquals("4a4d2f25c4fa4c4edb7a6d3c0d1aeca2f4f418ab", result.getChanges().get(2));
    }

    private void assertDateEquals(String expected, Date actual) {
        assertEquals(ZonedDateTime.parse(expected).toEpochSecond(), actual.getTime() / 1000);
    }

    private String readTestResource(String fileName) {
        File file = new File(this.getClass().getResource(fileName).getFile());
        try {
            return Files.asCharSource(file, Charsets.UTF_8).read();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read test resource: " + fileName, e);
        }
    }
}
