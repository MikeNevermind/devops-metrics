package com.atlassian.devops.build.jenkins;

import com.atlassian.devops.Config;
import org.junit.Test;


public class JenkinsClientTest {

    private static final String JENKINS_CONFIG_PROPS = "/com/atlassian/devops/build/jenkins/jenkins-config.properties";

    private JenkinsClient client = new JenkinsClient(new Config(JENKINS_CONFIG_PROPS), new JenkinsResponseParser());

    @Test
    public void testListBuildResults() {
        System.out.println(client.listBuildResults("test", null));
    }
}