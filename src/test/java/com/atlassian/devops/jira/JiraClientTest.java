package com.atlassian.devops.jira;

import com.atlassian.devops.Config;
import com.atlassian.devops.util.PageRequestImpl;
import org.junit.Test;

public class JiraClientTest {

    private JiraClient client = new JiraClient(new Config(), new JiraResponseParser());

    @Test
    public void testSearch() {
        client.search("project = BBSDEV AND type = bug AND resolutiondate >= -30d", new PageRequestImpl(20, 10));
    }
}