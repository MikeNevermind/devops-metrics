package com.atlassian.devops.jira;

import com.atlassian.devops.JiraIssue;
import com.atlassian.devops.util.Page;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class JiraResponseParserTest {

    private JiraResponseParser jiraResponseParser = new JiraResponseParser();

    @Test
    public void testShouldParseJiraIssueListData() {
        String jiraSearchResults = readTestResource("jira-search-result.json");

        Page<JiraIssue> page = jiraResponseParser.parseJiraIssueList(jiraSearchResults);

        assertNotNull(page);
        assertEquals(20, page.getStart());
        assertEquals(5, page.getLimit());
        assertEquals(5, page.getSize());
        assertFalse(page.isLastPage());

        List<JiraIssue> jiraIssues = page.getValues();
        assertNotNull(jiraIssues);
        assertEquals(5, jiraIssues.size());
        assertEquals("BBSDEV-19262", jiraIssues.get(0).getIssueKey());
        assertEquals("BBSDEV-19258", jiraIssues.get(1).getIssueKey());
        assertEquals("BBSDEV-19255", jiraIssues.get(2).getIssueKey());
        assertEquals("BBSDEV-19254", jiraIssues.get(3).getIssueKey());
        assertEquals("BBSDEV-19253", jiraIssues.get(4).getIssueKey());
    }

    private String readTestResource(String fileName) {
        File file = new File(this.getClass().getResource(fileName).getFile());
        try {
            return Files.asCharSource(file, Charsets.UTF_8).read();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read test resource: " + fileName, e);
        }
    }
}
